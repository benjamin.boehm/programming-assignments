# Teil 1 - Erstellung einer Einkaufsliste

In dieser Programmieraufgabe soll eine Anwendung entwickelt werden, bei der der Benutzer eine simple Einkaufsliste erstellen kann. Die Anwendung soll es dem Benutzer ermöglichen, die Einkaufsliste zu benennen, Artikel hinzuzufügen, Artikel von der Liste zu entfernen und (optional) diese Liste lokal zu speichern, sodass sie beim nächsten Besuch wieder vorhanden ist. Die Wahl der Programmiersprache oder ggf. von Frameworks ist frei.

## AnforderungenInteraktion

1. Eingabe des Namens der Einkaufsliste: Der Benutzer soll in der Lage sein, einen Namen für seine Einkaufsliste zu vergeben.
2. Eingabe von Artikeln: Der Benutzer sollte in der Lage sein, Artikelnamen einzugeben und sobald er das "Neu-Hinzufügen"-Eingabefeld verlässt, soll eine neue Zeile am Ende der Liste hinzugefügt werden als neues "Neu-Hinzufügen"-Eingabefeld.
3. Entfernen von Artikeln: Der Benutzer sollte in der Lage sein, einen Artikel aus der Einkaufsliste zu entfernen, falls er ihn nicht mehr benötigt.
4. Abhaken von Artikeln: Der Benutzer sollte in der Lage sein, einen Artikel zu markieren, wenn er diesen gekauft hat. Artikel, die als "abgehakt" markiert sind, sollen für das Eingabefeld die Hintergrundfarbe "`#fbfbfb`" erhalten und ihren Text durchgestrichen darstellen.
5. **(optional)** Testen: Kritische Benutzerinteraktionen und Funktionen sollten mit gerechtfertigten Tests abgedeckt werden.
6. **(optional)** Speichern der Einkaufsliste: Nach dem der Benutzer auf "Save" klickt, soll die Liste lokal für den Benutzer gespeichert werden und beim nächsten Besuch automatisch wieder geladen werden.
7. **(optional)** "Eigeninitiative": In den Anforderungen sind nicht alle sinnvollen Funktionalitäten abgebildet, daher sind weiter Optimierungen für ein stimmigeres Benutzererlebnis gern gesehen, aber sollten als solche gekennzeichnet werden.

## Demo interaktion

![Demo Interaktion](./demo-interaction.gif "Demo Interaktion")

_Hinweis: Felder im Hintergrund sind von Teil 2, für Teil 1 ignorieren_

## Hinweise

- Max. Aufwand **4 Stunden** nicht überschreiten.
- Diese Aufgabe ist "Teil 1 - Erstellung einer Einkaufsliste", ein imaginärer Teil 2 würde die Verwaltung von mehreren Listen (Anzeigen, Erstellen, Bearbeiten, nach Listenname suchen) enthalten. Dieses kann bei Überlegungen zu Datenstrukturen berücksichtigt werden.
- Grund-Styles für die Seite und für die nötigen Elemente sind bereitgestellt (siehe [TEMPLATES.md](TEMPLATES.md) und [structure.demo.html](structure.demo.html)). Sollten Anpassungen nötig/wünschenswert sein, sind diese gern willkommen.
