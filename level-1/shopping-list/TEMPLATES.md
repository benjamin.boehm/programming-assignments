# TEMPLATES

## Inputs

```html
<label class="input-label" aria-label="Label text for input">
  <input type="text" class="input-control" />
</label>
```

## Buttons

### Primary action button

```html
<button class="action-button action-button--secondary">
  Text
</button>
```

### Secondary action button

```html
<button class="action-button action-button--secondary">
  Text
</button>
```

### Icon action button

```html
<button
  class="action-button action-button--type-icon"
  title="remove entry"
>
  <!-- ICON: use size 16px x 16px -->
</button>
```

## SVGs

### Icon: "x"

```svg
<svg
  viewBox="0 0 24 24"
  xmlns="http://www.w3.org/2000/svg"
>
  <path
    d="M19 6.41 17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12 19 6.41z"
  />
</svg>
```
